# Architecture #

The application architecture is pretty simple:

 Pages                              Services                     Isolates

+---------------+                +-------------------+        +---------------+
|               |                |                   |        |               |
|               |                |                   +-------->               |
| PlayerPage    +----------------> PlayerService     |        | PlayerIsolate |
|               |                |                   <--------+               |
|         EVENTS|                |             EVENTS|        |               |
+-------------------------+      +---------+-^--------------+ +---------------+
                          |                | |              |
                          |                | |              |
+---------------+         |      +---------v-+-------+      | +---------------+
|               |         |      |                   |      | |               |
|               |         |      |                   +-------->               |
| SettingsPage  +----------------> ConfigService     |      | |    Settings   |
|               |         |      |                   <--------+    Isolate    |
|         EVENTS|         |      |             EVENTS|      | |               |
+-----------------------+ |      +---------+-^------------+ | +---------------+
                        | |                | |            | |
                        | |                | |            | |
+---------------+       | |      +---------v-+-------+    | | +---------------+
|               |       | |      |                   |    | | |               |
|               |       | |      |                   |    | | |               |
|    .......    |       | |      |    .......        |    | | |    .......    |
|               |       | |      |                   |    | | |               |
|               |       | |      |                   |    | | |               |
+---------------+       | |      +-------------------+--+ | | +---------------+
                        | |                             | | |
  +---------------------v-v-----------------------------v-v-v-----------------+
  |                                                                           |
  |                              Service Bus                                  |
  |                                                                           |
  +---------------------------------------------------------------------------+

(Diagram generated with [asciiflow](http://asciiflow.com/))

Any pieces of this architecture that you find missing are missing due to the incompleteness of the project. This page should reflect the intended architecture in-between major releases, and the codebase should always be striving to approach/maintain it.

# Pages #

A "Page" in the diagram represents the top level component of a [flutter `Navigator` route](https://docs.flutter.io/flutter/widgets/Navigator-class.html), along with all the components used on the page and associated machinery (`InheritedWidget`s, `State<T>`s, etc).

# Services #

Services manage domains of functionality in the app, as you see above the `PlayerService` deals with player-related concerns, like pausing playback or skipping a song. The `ConfigService` generally manages the configuration of the entire application (including other `Service`s inside it) and is a very large part of the functionality provided by the `SettingsPage`.

# Isolates #

Isolates are generally one-per-service, where services can perform operations that should not be attempted on the main thread. Long-running and short-running isolates might be used.

# Service Bus #

The service bus is a thinly wrapped [event bus](https://pub.dartlang.org/packages/event_bus), used by pages and services to wait on and respond to events in the system. While the [BLoC pattern](https://pub.dartlang.org/packages/flutter_bloc) and the [redux pattern](https://pub.dartlang.org/packages/redux) are attractive patterns provided to the Flutter community, this project uses a combination of direct method calls (when you have an instance) which return `Future`s where possible, and the event bus for other cases where more general asynchronicity is needed.
