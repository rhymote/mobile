# Rhymote Application #

<TODO: LOGO>

<TODO: SCREENSHOTS>

<TODO: FDROID DOWNLOAD LIINK>

[Flutter](https://flutter.io)-powered Mobile application for use with [a new Rhythmbox remote plugin I've developed](https://gitlab.com/rhymote/plugin).

# Dependencies #

0. [Flutter](https://flutter.io)
1. [Android SDK + build tooling](https://developer.android.com/studio/releases/build-tools)

Currently the mobile app is only tested on android, thought it could certainly be built for iOS, thanks to Flutter.

# Getting started #

To use the application:

1. Download a release (APK) (or build this project locally)
2.  and install it to your Android device

# Getting started (Development) #

To start hacking on this project:

0. Install all necessary dependencies, mostly [Flutter](https://flutter.io) and [Dart](https://www.dartlang.org)
1. Clone this repository
2. `flutter doctor` to ensure your setup is correct
3. `flutter run`

For more information on the application, check out the [`docs` directory](https://gitlab.com/rhymote/mobile/tree/master/docs).

# FAQ #

## What's being worked on? ##

Visit this project's issue tracker and [issue board](https://gitlab.com/rhymote/mobile/boards) for a view of what's coming up next.

## I want feature X! When will it arrive? ##

Make an issue! Interesting features motivate me (and others) to work on the project.

## I like this, how can I contribute? ##

There are lots of ways you can contribute to the project. In no particular order:

- Bug reports
- Feature requests
- Commit PRs
- Donate money for future development

## Why Flutter? Why not X? ##

This project was made in flutter purely on *my own personal whims*. After having done a project in [Nativescript-Vue](http://nativescript-vue.org/) which I liked a lot, I wanted to give Flutter a try.

It is entirely possible that this entire project will be completely rewritten in another framework (most likely Nativescript/Nativescript-Vue) if I decide that Flutter/Dart are no longer interesting.
