.PHONY: all build run flutter

FLUTTER=flutter

all: build 

flutter: 
	$(FLUTTER) --suppress-analytics $@

build:
	$(FLUTTER) --suppress-analytics build

run:
	$(FLUTTER) --suppress-analytics run
