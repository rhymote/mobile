import "package:flutter/material.dart";
import "package:flutter/services.dart";

import "services/player.dart";
import "services/config.dart";
import "services/connection.dart";
import "services/data.dart";

import "pages/player.dart";
import "pages/settings.dart";
import "pages/connections/create/direct_http.dart";

import "widgets/back_button_disabled.dart";
import "app_state.dart";

void main() => runApp(new RhymoteApp());

// Create top level services
final dataSvc = new DataService();
final playerSvc = new PlayerService();
final configSvc = new ConfigService(dataSvc: dataSvc);
final connSvc = new ConnectionService(dataSvc: dataSvc);

class RhymoteApp extends StatelessWidget {

    // This widget is the root of your application.
    @override
    Widget build(BuildContext context) {
        // final playerPage = new PlayerPageState(playerSvc: playerSvc, child: new DisabledBackButton(child: new PlayerPage()));
        final playerPage = new PlayerPageState(
            connSvc: connSvc,
            playerSvc: playerSvc,
            child: new PlayerPage(),
        );

        // Ensure system status bar is gray
        SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
            statusBarColor: Colors.grey,
        ));

        return new MaterialApp(
            title: 'Rhymote',
            theme: new ThemeData(
                primarySwatch: Colors.grey,
            ),
            routes: <String, WidgetBuilder> {
                "/": (BuildContext ctx) => playerPage,

                "/player": (BuildContext ctx) => playerPage,

                "/settings": (BuildContext ctx) => new AppState(
                    configSvc: configSvc,
                    connSvc: connSvc,
                    dataSvc: dataSvc,
                    child: new DisabledBackButton(child: new SettingsPage()),
                ),

                "/connections/create/direct-address": (BuildContext ctx) => new CreateDirectHTTPConnectionPage(connSvc: connSvc),
            },
        );
    }
}
