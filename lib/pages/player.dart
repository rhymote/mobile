import "package:flutter/material.dart";

import "../types.dart";
import "../errors.dart";
import "../services/player.dart";
import "../services/connection.dart";
import "../widgets/lhs_drawer.dart";
import "../widgets/title_row_with_icon.dart";
import "../widgets/player_controls.dart";

class PlayerPageState extends InheritedWidget {
    final PlayerService playerSvc;
    final ConnectionService connSvc;

    PlayerPageState({
        Key key,
        @required child,
        this.playerSvc,
        this.connSvc,
    }): super(key: key, child: child);

    static PlayerPageState of(BuildContext ctx) {
        return ctx.inheritFromWidgetOfExactType(PlayerPageState);
    }

    // Event buses wil be used to synchronize services
    @override
    bool updateShouldNotify(PlayerPageState old) => false;
}

class PlayerPage extends StatefulWidget {
    final bool playerHasActiveConnections;

    PlayerPage({Key key, this.playerHasActiveConnections}) : super(key: key);

    @override
    _PlayerPageState createState() => new _PlayerPageState();
}

class _PlayerPageState extends State<PlayerPage> {
    bool _loading = true;
    ConnectionConfig _activeConnection;

    @override
    void didChangeDependencies(){
        super.didChangeDependencies();

        final PlayerPageState state = PlayerPageState.of(context);

        // Initial load to try and find an active connection
        state.connSvc
            .getActiveConnection()
            .then((conn) {
                // Update the active connection
                setState(() {
                    _loading = false;

                    if (conn.isPresent) {
                        _activeConnection = conn.value;
                    }
                });
            });

        // Update active connection when it's changed/updated
        state.connSvc
            .eventBus
            .on<ActiveConnectionChanged>()
            .listen((event) {
                setState(() { _activeConnection = event.conn; });
            });
    }

    /// Show loading indicator
    static Widget _buildLoadingUI() {
        return new Center(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                    new Text("Loading..."),
                    const SizedBox(height: 20.0,),
                    new CircularProgressIndicator(),
                ]
            ),
        );
    }

    void _sendPlayerCmd(BuildContext ctx, PlayerCmd cmd) {
        final PlayerPageState state = PlayerPageState.of(context);
        state.playerSvc.execute(_activeConnection, cmd);
    }

    Widget _buildPlayerUI(BuildContext ctx) {
        return new Center(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                    new Text("Has active conn!"),

                    new PlayerControls(
                        onNextPress: () => _sendPlayerCmd(ctx, PlayerCmd.next),
                        onStopPress: () => _sendPlayerCmd(ctx, PlayerCmd.stop),
                        onPreviousPress: () => _sendPlayerCmd(ctx, PlayerCmd.previous),
                        onPlayPausePress: () => _sendPlayerCmd(ctx, PlayerCmd.play_pause),
                    ),
                ],
            ),
        );
    }

    Widget _buildNoActiveConnectionUI(BuildContext ctx) {
        return new Center
        (
            child: new Column
            (
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                    new Text("No active Rhythmbox connections"),
                    new SizedBox(height: 20.0),
                    new RaisedButton
                    (
                        padding: EdgeInsets.all(10.0),
                        child: new Column(children: [
                            new Icon(Icons.settings),
                            new Text("Settings"),
                        ]),
                        onPressed: () => Navigator.popAndPushNamed(ctx, "/settings"),
                    ),

                ]
            ),
        );
    }

    @override
    Widget build(BuildContext ctx) {

        Widget body;
        if (this._loading) {
            body = _buildLoadingUI();
        } else {
            body = _activeConnection == null ?  _buildNoActiveConnectionUI(ctx) : _buildPlayerUI(ctx);
        }

        return new Scaffold(
            appBar: new AppBar(title: new TitleRowWithIcon(icon: Icons.speaker, text: "Player")),
            drawer: new Drawer(child: new LHSDrawer()),
            body: body
        );

    }
}
