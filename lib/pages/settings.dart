import "package:flutter/material.dart";
import "package:optional/optional.dart";
import "../types.dart";
import "../app_state.dart";

import "../services/config.dart";
import "../services/connection.dart";

import "./connections/choose_type.modal.dart";
import "../widgets/lhs_drawer.dart";
import "../widgets/title_row_with_icon.dart";
import "../widgets/config_sub_section_title.dart";
import "../widgets/connection_listing.dart";

class SettingsPage extends StatefulWidget {
    SettingsPage({Key key}) : super(key: key);

    @override
    _SettingsPageState createState() => new _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
    AppConfig _appCfg;
    String _activeConnectionName;
    bool _loading = true;
    List<ConnectionConfig> _connections;

    void _finishInitialLoad(AppConfig cfg) {
        setState(() {
            _loading = false;
            _appCfg = cfg;
        });
    }

    @override
    void didChangeDependencies(){
        super.didChangeDependencies();

        // Expecting to be under an AppState (InheritedWidget)
        AppState appState = AppState.of(context);

        // Get original stateGet initial state
        appState.configSvc
            .getConfig()
            .then(this._finishInitialLoad);

        // Register listener on the connection service listing
        appState.connSvc
            .eventBus
            .on<ConnectionAddedEvent>()
            .listen((_) {
                // re-fetch the connection list
                appState
                    .connSvc
                    .getConnections()
                    .then((cs) {
                        setState(() { _connections = cs; });
                    });
            });

        // Update active connection when it's changed/updated
        appState.connSvc
            .eventBus
            .on<ActiveConnectionChanged>()
            .listen((event) {
                setState(() { _activeConnectionName = event.value; });
            });
    }

    Future<String> _showConfirmEraseDialog(BuildContext ctx) {
        // Show success toast/notification
        return showDialog(
            context: ctx,
            builder: (_) => new AlertDialog(
                title: new Text("Data reset"),
                content: new Text("Are you sure you want to delete all local data?"),
                actions: <Widget>[
                    new FlatButton(
                        child: Text('Cancel'),
                        onPressed: () => Navigator.pop(ctx, null)
                    ),
                    new FlatButton(
                        child: Text('Erase'),
                        color: Colors.red,
                        textColor: Colors.white,
                        onPressed: () => Navigator.pop(ctx, "Yes")
                    ),
                ],
            ),
        );
    }

    Future<String> _showEraseCompletedDialog(BuildContext ctx) {
        // Show success toast/notification
        return showDialog(
            context: ctx,
            builder: (_) => new AlertDialog(
                title: new Text("Data reset"),
                content: new Text("Data has been cleared"),
                actions: <Widget>[
                    new FlatButton(
                        child: Text('Close'),
                        onPressed: () => Navigator.pop(ctx, null)
                    ),
                ],
            ),
        );
    }

    Widget _buildGeneralTabView(BuildContext ctx) {
        AppState appState = AppState.of(context);

        // TODO: get the current value of all user options for the general tab

        return new ListView(
            padding: EdgeInsets.all(10.0),
            children: <Widget>[
                // Wifi settings
                new ConfigSubSectionTitle(text: "Wifi"),
                new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                        new Text("Only Wifi only for large downloads"),
                        new Switch(
                            value: false,
                            onChanged: (value) {
                                appState
                                    .configSvc
                                    .setUserOption(UserOption.largeDownloadsOnlyOnWifi, value);
                            }),
                    ]
                ),

                // Wifi settings
                new ConfigSubSectionTitle(text: "Data"),
                new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                        new Text("Delete all data"),
                        new RaisedButton(
                            child: new Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                    new Icon(Icons.delete),
                                    new Text("Erase"),
                                ]
                            ),
                            color: Colors.red,
                            textColor: Colors.white,
                            onPressed: () {
                                // TODO: show confirm dialog
                                this._showConfirmEraseDialog(ctx)
                                    .then((res) {
                                        // Only do anything with Yeses
                                        if (res != "Yes") { return; }

                                        appState
                                            .dataSvc
                                            .resetDB()
                                            .then((_) {
                                                // Show dialog to let user know it's been deleted
                                                this._showEraseCompletedDialog(ctx);
                                            });
                                    });
                            },
                        ),
                    ]
                ),

            ],
        );
    }

    /// Get or fetch the list of connections
    Future<List<ConnectionConfig>> _getOrFetchConnections(ConnectionService connSvc) {
        return connSvc
            .getConnections()
            .then((connections) {
                // If we've loaded and are storing connections then use that
                if (_connections != null) {
                    return this._connections;
                }

                return connections;
            });
    }

    Widget _buildConnectionsTabView(BuildContext ctx) {
        // Expecting to be under an AppState (InheritedWidget)
        AppState appState = AppState.of(context);

        // Create future that presents the list of connections and the active one
        final Future<ConnectionListWithActive> clwa = _getOrFetchConnections(appState.connSvc)
            .then((connections) => new ConnectionListWithActive(
                connections,
                active: Optional.ofNullable(_activeConnectionName)
            ));

        return new Scaffold(
            body: new ListView(
                padding: EdgeInsets.all(10.0),
                children: <Widget>[
                    new ConfigSubSectionTitle(text: "Available Connections"),
                    new SizedBox(height: 10.0),
                    new ConnectionListing(
                        connectionListWithActive: clwa,
                        onSelected: (conn) {
                            appState
                                .connSvc
                                .setActiveConnection(conn)
                            // Update the active connection
                                .then((updatedConn) {
                                    setState(() {
                                        _activeConnectionName = updatedConn.getName();
                                    });
                                })
                            // Handle errors
                                .catchError((err) {
                                    // TODO: show error dialog
                                    debugPrint("[SettingsPage] ERROR:\n $err");
                                });
                        },
                    ),
                ],
            ),

            floatingActionButton: new FloatingActionButton(
                child: new Icon(Icons.add),
                onPressed: () {
                    // Navigate to modal page to get the desired connection type
                    Navigator
                        .push(ctx, MaterialPageRoute<ConnectionType>(
                            builder: (_) => new ChooseConnectionTypeModalPage()
                        )).then((ConnectionType cType) {
                            // If back button is pressed cType will be null
                            if (cType == null)
                                return;

                            switch (cType){
                                case ConnectionType.direct_address:
                                    Navigator.pushNamed(ctx, "/connections/create/direct-address");
                                    break;
                                default:
                                    // Show error for unsupported connection type
                                    showDialog(
                                        context: ctx,
                                        builder: (_) => new AlertDialog(
                                            title: Text("Not Supported"),
                                            content: Text("The connection type you've chosen is not supported"),
                                            actions: <Widget>[
                                                FlatButton(
                                                    child: Text('OK'),
                                                    onPressed: () => Navigator.of(context).pop(),
                                                ),
                                            ],
                                        ),
                                    );
                            }

                        })
                        .catchError((err) {
                            // SHOW ERROR TOAST, redirect?
                            debugPrint("ERROR:\n$err");
                        });
                },
            ),
        );
    }

    @override
    Widget build(BuildContext ctx) {
        return new DefaultTabController(
            length: 2,
            child: new Scaffold(

                appBar: new AppBar(
                    title: new TitleRowWithIcon(icon: Icons.settings, text: "Settings"),

                    bottom: new TabBar(
                        tabs: [
                            new Tab(text: "General"),
                            new Tab(text: "Connections"),
                        ],
                    )
                ),

                drawer: new Drawer(child: new LHSDrawer()),

                body: new TabBarView(
                    children: <Widget>[
                        _buildGeneralTabView(ctx),
                        _buildConnectionsTabView(ctx),
                    ]
                ),
            )
        );

    }
}
