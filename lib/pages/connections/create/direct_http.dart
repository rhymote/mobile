import "dart:math";
import "package:flutter/material.dart";
import "../../../services/connection.dart";
import "../../../widgets/lhs_drawer.dart";
import "../../../widgets/title_row_with_icon.dart";
import "../../../widgets/config_sub_section_title.dart";
import "../../../widgets/form_fields/port.dart";
import "../../../widgets/form_fields/hostname.dart";
import "../../../types.dart";

class CreateDirectHTTPConnectionPageState extends InheritedWidget {
    final ConnectionService connSvc;

    CreateDirectHTTPConnectionPageState({
        Key key,
        @required child,
        this.connSvc,
    }): super(key: key, child: child);

    static CreateDirectHTTPConnectionPageState of(BuildContext ctx) {
        return ctx.inheritFromWidgetOfExactType(CreateDirectHTTPConnectionPageState);
    }

    // Event buses wil be used to synchronize services
    @override
    bool updateShouldNotify(CreateDirectHTTPConnectionPageState old) => false;
}

class CreateDirectHTTPConnectionPage extends StatefulWidget {
    final ConnectionService connSvc;

    CreateDirectHTTPConnectionPage({Key key, this.connSvc}) : super(key: key);

    @override
    _CreateDirectHTTPConnectionPageState createState() => new _CreateDirectHTTPConnectionPageState(this.connSvc);
}

class _CreateDirectHTTPConnectionPageState extends State<CreateDirectHTTPConnectionPage> {
    final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

    ConnectionService connSvc;

    // DirectHTTPConnectionConfig fields
    String name;
    String hostname;
    int port;

    _CreateDirectHTTPConnectionPageState(this.connSvc);

    Future<bool> _showTestSuccessDialog(BuildContext ctx) {
        // Show success toast/notification
        return showDialog(
            context: ctx,
            builder: (_) => new AlertDialog(
                title: Text("Test Succeeded"),
                content: Text("Successfully connected to the specified address."),
                actions: <Widget>[
                    FlatButton(
                        child: Text('OK'),
                        onPressed: () => Navigator.of(context).pop(),
                    ),
                ],
            ),
        );
    }

    Future<bool> _showUnexpectedErrorDialog(BuildContext ctx) {
        // Show success toast/notification
        return showDialog(
            context: ctx,
            builder: (_) => new AlertDialog(
                title: Text("Unexpected Error"),
                content: Text("An Unexpected error has occurred. Please report if it persists."),
                actions: <Widget>[
                    FlatButton(
                        child: Text('OK'),
                        onPressed: () => Navigator.of(context).pop(),
                    ),
                ],
            ),
        );
    }

    Future<bool> _showTestFailureDialog(BuildContext ctx) {
        showDialog(
            context: ctx,
            builder: (_) => new AlertDialog(
                title: Text("Test Failed"),
                content: Text("Unable to connect to the specified address."),
                actions: <Widget>[
                    FlatButton(
                        child: Text('OK'),
                        onPressed: () => Navigator.of(context).pop(),
                    ),
                ],
            ),
        );
    }

    bool _validateAndSave() {
        final result = this._formKey.currentState.validate();
        if (result) { this._formKey.currentState.save(); }
        return result;
    }

    DirectHTTPConnectionConfig _makeConnection() {
        return new DirectHTTPConnectionConfig(
            name: this.name,
            hostname: this.hostname,
            port: this.port,
        );
    }

    /// Test a direct  connection
    void testConnectionDetails(BuildContext ctx) {
        if (!this._validateAndSave()) { return; }

        final conn = this._makeConnection();

        this.connSvc
            .testConnection(conn)
        // Successful PING using connection info
            .then((resp) {
                // Test connection failure
                if (!resp.succeeded) {
                    // TODO: probably pass along the error here or stuff a user-readable error msg in response
                    this._showTestFailureDialog(ctx);
                    return;
                }

                this._showTestSuccessDialog(ctx);
            })
        // Failure in connection checking code
            .catchError((_) => this._showUnexpectedErrorDialog(ctx));
    }

    /// Save the new connection
    void saveNewConnection(BuildContext ctx) {
        if (!this._validateAndSave()) { return; }

        ConnectionConfig newConn = this._makeConnection();

        this.connSvc
            .saveConnection(newConn)
            .then((_) => Navigator.of(ctx).pop());
    }

    @override
    Widget build(BuildContext ctx) {
        return new Scaffold(
            body: new Container(
                padding: EdgeInsets.all(10.0),
                child: new Form(
                    key: this._formKey,
                    child: new ListView(
                        children: <Widget>[
                            new ConfigSubSectionTitle(text: "Direct HTTP Connection"),

                            new TextFormField(
                                decoration: new InputDecoration(
                                    hintText: "ex. Laptop, Bob's Desktop",
                                    labelText: " Connection name",
                                ),
                                onSaved: (v) => this.name = v.trim()
                            ),

                            new SizedBox(height: 20.0),
                            new HostnameFormField(onSaved: (v) => this.hostname = v.trim()),
                            new SizedBox(height: 20.0),
                            new PortFormField(onSaved: (v) => this.port = int.parse(v.trim())),

                            new SizedBox(height: 40.0),

                            // Action buttons
                            new Column(
                                children: <Widget>[
                                    new FractionallySizedBox(
                                        widthFactor: 0.8,
                                        child: new RaisedButton(
                                            child: new Row(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                children: <Widget>[
                                                    new Icon(Icons.build),
                                                    new Text(" Test connection"),
                                                ]
                                            ),
                                            color: Colors.green,
                                            textColor: Colors.white,
                                            onPressed: () => this.testConnectionDetails(ctx),
                                        ),
                                    ),

                                    new SizedBox(width: 10.0),

                                    new FractionallySizedBox(
                                        widthFactor: 0.8,
                                        child: new RaisedButton(
                                            child: new Row(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                children: <Widget>[
                                                    new Icon(Icons.save),
                                                    new Text(" Save"),
                                                ]
                                            ),
                                            color: Colors.blue,
                                            textColor: Colors.white,
                                            onPressed: () => this.saveNewConnection(ctx),
                                        ),
                                    ),
                                ]
                            ),
                        ]
                    ),
                ),
            ),
        );
    }
}
