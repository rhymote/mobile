import "package:flutter/material.dart";
import "../../widgets/config_sub_section_title.dart";
import "../../types.dart";

/// Page for choosing a connection type
///
/// This widget should normally be used as a modal that returns a value (the selected modal type)
class ChooseConnectionTypeModalPage extends StatelessWidget {
    @override
    Widget build(BuildContext ctx) {
        // TODO: Create an option for every modal type

        return new Scaffold(
            body: new Container(
                padding: EdgeInsets.all(10.0),
                child: new ListView(
                    children: <Widget>[
                        // TODO: Page title container (needs to be main color from theme?)
                        new Text("Choose a connection type"),

                        new SizedBox(height: 30.0),

                        new Text("Automatic", style: TextStyle(fontSize: 20.0)),

                        new ListTile(
                            leading: new Icon(Icons.photo_camera),
                            title: new Text("QR Code scan"),
                            onTap: () => Navigator.pop(ctx, ConnectionType.qr_code)
                        ),

                        new Text("Manual", style: TextStyle(fontSize: 20.0)),

                        new ListTile(
                            leading: new Icon(Icons.computer),
                            title: new Text("Direct HTTP (IP/hostname)"),
                            onTap: () => Navigator.pop(ctx, ConnectionType.direct_address)
                        ),

                        new ListTile(
                            leading: new Icon(Icons.cloud),
                            title: new Text("Proxy"),
                            onTap: () => Navigator.pop(ctx, ConnectionType.proxy)
                        ),

                        new SizedBox(height: 20.0),
                    ]
                ))
        );
    }
}
