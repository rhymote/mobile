import "package:flutter/material.dart";

class PlayerControls extends StatelessWidget {
    final Function onPlayPausePress;
    final Function onStopPress;
    final Function onNextPress;
    final Function onPreviousPress;

    const PlayerControls({
        Key key,
        this.onPlayPausePress,
        this.onStopPress,
        this.onNextPress,
        this.onPreviousPress,
    }) : super(key: key);

    @override
    Widget build(BuildContext ctx) {
        final iconSize = 50.0;

        return new Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
                new IconButton(
                    icon: new Icon(Icons.skip_previous),
                    iconSize: iconSize,
                    tooltip: "Previous",
                    onPressed: this.onPreviousPress,
                ),

                new IconButton(
                    icon: new Icon(Icons.stop),
                    iconSize: iconSize,
                    tooltip: "Stop",
                    onPressed: this.onStopPress,
                ),

                new IconButton(
                    icon: new Icon(Icons.play_arrow),
                    iconSize: iconSize,
                    tooltip: "Play/Pause",
                    onPressed: this.onPlayPausePress,
                ),

                new IconButton(
                    icon: new Icon(Icons.skip_next),
                    iconSize: iconSize,
                    tooltip: "Next",
                    onPressed: this.onNextPress,
                ),

            ]
        );
    }
}
