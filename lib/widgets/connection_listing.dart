import "package:flutter/material.dart";
import "../types.dart";
import "../services/connection.dart";

class ConnectionListing extends StatelessWidget {
    final Future<ConnectionListWithActive> connectionListWithActive;
    final Function onSelected;

    const ConnectionListing({Key key, this.connectionListWithActive, this.onSelected}) : super(key: key);

    @override
    Widget build(BuildContext ctx) {
        return new FutureBuilder<ConnectionListWithActive>(
            future: this.connectionListWithActive,
            builder: (BuildContext ctx, AsyncSnapshot<ConnectionListWithActive> snapshot) {

                switch (snapshot.connectionState) {
                    case ConnectionState.none:
                        return Text('No connections.');

                    case ConnectionState.active:
                    case ConnectionState.waiting:
                        return new Container(
                            padding: EdgeInsets.all(20.0),
                            child: new Column(
                                children: <Widget>[
                                    new Text('Loading'),
                                    new CircularProgressIndicator(),
                                ]
                            ));

                    case ConnectionState.done:
                        // Error case
                        if (snapshot.hasError) {
                            return new Container(
                                padding: EdgeInsets.all(20.0),
                                child: new Text('Error: ${snapshot.error}')
                            );
                        }


                        // CAREFUL: code below assumes that empty names are not allowed
                        final connections = snapshot.data.connections;
                        final activeConnectionName = snapshot.data.active.isPresent ? snapshot.data.active.value : "";

                        // Handle empty
                        if (connections.isEmpty) {
                            return new Container(
                                height: 200.0,
                                decoration: new BoxDecoration(
                                    border: Border.all(width: 5.0, color: Colors.grey),
                                ),
                                child: new Center(child: new Text("No connections available")),
                            );
                        }

                        final leadingIcon = new Icon(Icons.cast);

                        // Likely to be a small list of connections so we'll just pre-compute the tiles up front
                        final listTiles = connections.map((c) => new ListTile(
                            leading: leadingIcon,
                            title: new Text(c.getName()),
                            trailing: activeConnectionName == c.getName() ? new Icon(Icons.power) : null,
                            onTap: () => this.onSelected(c),
                        )).toList();

                        return new Container(
                            height: 200.0,
                            decoration: new BoxDecoration(
                                border: Border.all(width: 5.0, color: Colors.grey),
                            ),
                            child: new ListView(children: listTiles),
                        );
                }
            });
    }
}
