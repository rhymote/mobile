import "package:flutter/material.dart";

class TitleRowWithIcon extends StatelessWidget {
    final IconData icon;
    final String text;

    const TitleRowWithIcon({Key key, this.icon, this.text }) : super(key: key);

    @override
    Widget build(BuildContext ctx) {
        return new Row(
            children: <Widget>[
                new Icon(this.icon),
                new Text(this.text)
            ]
        );
    }
}
