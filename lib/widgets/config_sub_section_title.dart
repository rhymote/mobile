import "package:flutter/material.dart";

/// Configuration sub-section title
class ConfigSubSectionTitle extends StatelessWidget {
    final String text;

    const ConfigSubSectionTitle({Key key, String this.text}) : super(key: key);

    @override
    Widget build(BuildContext ctx) {
        return new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
                new Text(text, style: new TextStyle(fontSize: 20.0)),
                new FractionallySizedBox(
                    widthFactor: 0.1,
                    child: new Divider(color: Colors.black, height: 15.0)
                ),
            ],
        );
    }
}
