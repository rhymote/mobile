import "package:flutter/material.dart";

class PortFormField extends StatelessWidget {
    final FormFieldSetter<String> onSaved;
    static RegExp number_regex = new RegExp(r"[0-9]+");

    PortFormField({Key key, this.onSaved}) : super(key: key);

    String _validatePortNumber(String value) {
        if (!PortFormField.number_regex.hasMatch(value)) { return "Invalid port number"; }
        return null;
    }

    @override
    Widget build(BuildContext ctx) {
        return new FractionallySizedBox(
            alignment: Alignment.topLeft,
            widthFactor: 0.5,
            child: new TextFormField(
                keyboardType: new TextInputType.numberWithOptions(),
                validator: this._validatePortNumber,
                onSaved: this.onSaved,
                decoration: new InputDecoration(
                    hintText: "ex. 5678",
                    labelText: "Port Number",
                ),
            ),
        );
    }
}
