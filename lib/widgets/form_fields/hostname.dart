import "package:flutter/material.dart";

class HostnameFormField extends StatelessWidget {
    final FormFieldSetter<String> onSaved;

    HostnameFormField({Key key, this.onSaved}) : super(key: key);

    @override
    Widget build(BuildContext) {
        return new TextFormField(
            decoration: new InputDecoration(
                hintText: "ex. 192.168.1.10 or computer.ddns-provider.com",
                labelText: " Hostname",
            ),
            onSaved: this.onSaved,
        );
    }
}
