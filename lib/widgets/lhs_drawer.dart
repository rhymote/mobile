import "package:flutter/material.dart";

class LHSDrawer extends StatelessWidget {
    const LHSDrawer({Key key}) : super(key: key);

    @override
    Widget build(BuildContext ctx) {
        return new ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
                // Clear the top of the ap
                new SizedBox(height: 40.0),

                // Player button
                new ListTile
                (
                    leading: new Icon(Icons.speaker),
                    title: new Text("Player", style: TextStyle(fontSize: 25.0)),
                    onTap: () {
                        // TODO: detect if the page is the current route and only do the pop (extend/use custom Navigator?)
                        Navigator.popAndPushNamed(ctx, "/player");
                    }
                ),

                // Settings button
                new ListTile
                (
                    leading: new Icon(Icons.settings),
                    title: new Text("Settings", style: TextStyle(fontSize: 25.0)),
                    onTap: () {
                        // TODO: detect if the page is the current route and only do the pop.
                        Navigator.popAndPushNamed(ctx, "/settings");
                    }
                )

            ],

        );
    }
}
