import "package:flutter/material.dart";

class DisabledBackButton extends WillPopScope {
  DisabledBackButton({Key key, @required child}): super(key: key, child: child, onWillPop: () {});
}
