import "../types.dart";

final migrations = [
    new DBMigration(
        version: 1,
        sql: """
CREATE TABLE kv (
  key TEXT PRIMARY KEY,
  value TEXT,
  created_at TEXT NOT NULL DEFAULT (datetime()),
  updated_at TEXT NOT NULL DEFAULT (datetime())
);

CREATE TRIGGER IF NOT EXISTS kv_updated_at
BEFORE UPDATE ON kv
FOR EACH ROW
BEGIN UPDATE
UPDATE kv SET updated_at=datetime() WHERE key = NEW.key;
END;

"""),

    new DBMigration(
        version: 2,
        sql: """
CREATE TABLE connection_configs (
  uuid TEXT PRIMARY KEY,
  name TEXT NOT NULL,                   -- Name of the connection (user provided)
  _type TEXT NOT NULL,                  -- Type of connection (ex. `ConnectionType.direct_address`)
  json TEXT NOT NULL,                   -- Serialized connection information
  created_at TEXT DEFAULT (datetime()), -- Creation time
  updated_at TEXT DEFAULT (datetime())  -- Update time
);

CREATE TRIGGER IF NOT EXISTS connections_updated_at
BEFORE UPDATE ON connections
FOR EACH ROW
BEGIN UPDATE
UPDATE connections SET updated_at=datetime() WHERE name = NEW.name;
END;
"""),

];
