class NoSuchIsolateError implements Exception {
    String cause = "No such isolate exists";
    NoSuchIsolateError({this.cause});
}

class NoSuchConnectionError implements Exception {
    String cause = "No such connection exists";
    NoSuchConnectionError({this.cause});
}

class UnexpectedError implements Exception {
    String cause = "Unexpected error";
    UnexpectedError({this.cause});
}
