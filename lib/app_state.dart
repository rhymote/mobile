import "package:flutter/material.dart";
import "./services/config.dart";
import "./services/connection.dart";
import "./services/data.dart";

class AppState extends InheritedWidget {
    final ConfigService configSvc;
    final ConnectionService connSvc;
    final DataService dataSvc;

    AppState({
        Key key,
        @required child,
        this.configSvc,
        this.connSvc,
        this.dataSvc,
    }): super(key: key, child: child);

    static AppState of(BuildContext ctx) {
        return ctx.inheritFromWidgetOfExactType(AppState);
    }

    // Event buses wil be used to synchronize services
    @override
    bool updateShouldNotify(AppState old) => false;
}
