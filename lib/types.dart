import "dart:isolate";
import "dart:convert";

import "package:event_bus/event_bus.dart";
import "package:uuid/uuid.dart";
import "package:optional/optional.dart";

final uuidGenerator = new Uuid();

/// Config for the entire application
class AppConfig {
    final String name;
    AppConfig(this.name);
}

enum UserOption {
    largeDownloadsOnlyOnWifi,
}

class IsolateAndPort {
    Isolate isolate;
    ReceivePort rp;

    IsolateAndPort({this.isolate, this.rp});
}

/// Service service
// TODO: abstract over Command type <C> (so Service<CMD extends Command, Response extends Response, Event extends Event>),
// this should should allow generic implementation of send/receive message, and event bus implementations
// maybe even include isolate function? as a part of the abstract class? by default only include an isolate that does nothing?
// or expose a processCommand, which can be overriden to handle the isolate spawning stuff
abstract class Service {
    EventBus eventBus;
    Map<String, IsolateAndPort> isolates;

    Service() {
        this.eventBus = new EventBus();
        this.isolates = new Map<String, IsolateAndPort>();
    }

    Iterable<Isolate> getIsolates() => this.isolates.values.map((iap) => iap.isolate);
}

enum ConnectionType {
    qr_code,
    direct_address,
    proxy,
}

abstract class ConnectionConfig {
    ConnectionType getConnectionType();
    Future<String> toUrl();

    String getName();
    dynamic getConfigJson();
}

class DirectHTTPConnectionConfig extends Rowable implements ConnectionConfig {
    ConnectionType _type = ConnectionType.direct_address;
    String name;
    String hostname;
    int port;

    DirectHTTPConnectionConfig({this.name, this.hostname, this.port});

    DirectHTTPConnectionConfig.fromRow(Map<String, dynamic> row) {
        this.name = row["name"];
        this._type = ConnectionType.values.firstWhere((e) => e.toString() == row["_type"]);

        final json = jsonDecode(row["json"]);
        this.hostname = json["hostname"];
        this.port = json["port"];
    }

    ConnectionType getConnectionType() => this._type;

    @override
    String getName() => this.name;

    @override
    Future<String> toUrl() => Future.value("http://${this.hostname}:${this.port}");

    @override
    dynamic getConfigJson() {
        return {
            "name": this.name,
            "hostname": this.hostname,
            "port": this.port,
            "_type": this._type.toString(),
        };
    }

    /// Has to conform to ConnectionConfig for storage in connection_configs table
    List<String> insertColumns() => ["name", "_type", "json"];

    /// Has to conform to ConnectionConfig for storage in connection_configs table
    List<dynamic> insertValues() => [this.name, this._type.toString(), jsonEncode(this.getConfigJson())];
}

enum ServiceIsolateType {
    general,
}


class EntityId<T> {
    String uuid;

    EntityId({this.uuid}) {
        if (this.uuid == null) {
            this.uuid = uuidGenerator.v4();
        }
    }
}

class PaginationOptions {
    int limit;
    int offset;
}

enum EntityType {
    connection_config,
}

/// There *has* to be a better way of storing this relatively static information closer to the types of entities.
/// So far I haven't foudn a clean composable to do this in dart... I'm not a fan of this language.

String getTableNameForEntityType(EntityType et) {
    switch(et) {
        case EntityType.connection_config:
            return "connection_configs";
        default:
            return null; // yikes
    }
}

String getTableNameForTypeEnum(String value) {
    if (value == ConnectionType.direct_address.toString()) {
        return "connection_configs";
    }

    return null; // yikes
}

Function getRowConstructorForTypeEnum(String value) {
    if (value == ConnectionType.direct_address.toString()) {
        return (r) => DirectHTTPConnectionConfig.fromRow(r);
    }

    return null; // yikes
}

List<String> insertColumnsForTypeEnum(String value) {
    if (value == ConnectionType.direct_address.toString()) {
        return ["name","_type", "json"]; // (created|updated)_at are managed by the DB
    }

    return null; // yikes
}

// TODO: find the right Function subclass to use here -- no idea in the docs where to find this
Function insertValuesForEntityType(EntityType et) {
    switch(et) {
        case EntityType.connection_config:
            return (ConnectionConfig c) => [c.getName(), et.toString(), jsonEncode(c.getConfigJson())];
        default:
            return null; // yikes
    }
}

class Entity<T extends Rowable> implements Rowable {
    EntityId<T> id;
    T data;

    Entity(T this.data, {this.id}) {
        if (this.id == null) {
            this.id = new EntityId<T>();
        }
    }

    EntityType getEntityType() {
        if (this.data is ConnectionConfig) { return EntityType.connection_config; }
        throw new StateError("Invalid/Unrecognized entity");
    }

    List<String> insertColumns() {
        final columns = this.data.insertColumns();
        columns.insert(0, "uuid");
        return columns;
    }

    List<dynamic> insertValues() {
        final values = this.data.insertValues();
        values.insert(0, this.id.uuid);
        return values;
    }
}

abstract class Rowable {
    List<String> insertColumns();
    List<dynamic> insertValues();
}

class DBMigration {
    int version;
    String sql;

    DBMigration({this.version, this.sql});
}

class ConnectionListWithActive {
    List<ConnectionConfig> connections;
    Optional<String> active;

    ConnectionListWithActive(this.connections, {this.active});
}
