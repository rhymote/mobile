import "dart:isolate";
import "dart:async";

import "package:http/http.dart" as http;
import "package:optional/optional.dart";

import "../types.dart";
import "../errors.dart";

enum PlayerCmd {
    ping,

    stop,
    play_pause,
    next,
    previous,
}

/// Configuration for the player service
class PlayerServiceConfig {
    final String name;
    final List<ConnectionConfig> _connectionCfg;

    PlayerServiceConfig(this.name, this._connectionCfg);
}

/// Service for handling requests to the player.
///
/// This service maintains one or more connections for the current player
class PlayerService extends Service {
    // Mapping of ConnectionType to isolates which can send commands to the specific connection type
    Map<ConnectionType, Isolate> _connectionIsolates = {};
    PlayerServiceConfig _cfg;

    set cfg(PlayerServiceConfig c) {
        // TODO: clear out resources used by the service up until now
        // this.refreshState(); trigger this?
        this._cfg = c;
    }

    /// Test a connection (by sending a PlayerCmd.ping)
    Future<bool> testConnection(ConnectionConfig c) {
        return this.execute(c, PlayerCmd.ping);
    }

    // /// Clear transient state linked to current configuration
    // ///
    // Future<void> _clearState() {
    //     return this._removeAllConnections()
    //         .then((_) => this._shutdownAllIsolates());
    // }

    // /// Shut down all isolates
    // ///
    // /// This would normally be done when switching out the current player configuration.
    // Future<void> _shutdownAllIsolates() {
    //     // TODO: shutdown all isolates
    //     return Future<void>.error(UnimplementedError);
    // }

    Optional<String> _getResourceUrlForCmd(PlayerCmd cmd) {
        switch (cmd) {
            case PlayerCmd.play_pause: return Optional.of("commands/playback-play-pause");
            case PlayerCmd.stop: return Optional.of("commands/playback-stop");
            case PlayerCmd.next: return Optional.of("commands/playback-next");
            case PlayerCmd.previous: return Optional.of("commands/playback-previous");
            default:
                return Optional.ofNullable(null);
        }
    }

    /// Execute a player command
    ///
    /// Under the covers this means finding an open/working connection and passing along the command
    /// to the appropriate isolate to run in the background, and waiting for the result.
    Future<void> execute(ConnectionConfig conn, PlayerCmd cmd) async {
        // TODO: Find an open connection
        // TODO: find or create an isolate to run the task
        // TODO: send message to isolate

        // Hard coded send of command
        if (conn is DirectHTTPConnectionConfig) {
            final url = _getResourceUrlForCmd(cmd);

            if (!url.isPresent) {
                final err = new UnexpectedError(cause: "No POST URL for command [${cmd.toString()}] (DirectHTTPConnection)");
                return Future.error(err);
            }

            final baseUrl = await conn.toUrl();
            final fullUrl = "${baseUrl}/${url.value}";
            print("[PlayerService] Sending command [${cmd.toString()}] to [${fullUrl}]");

            await http.post(fullUrl)
                .then((resp) {
                    print("[PlayerService] resp: $resp.toString()");
                })
                .catchError((err) {
                    print("[PlayerService] ERROR:\n $err");
                });

            return Future.value();
        }

        throw new UnimplementedError();
    }

}
