import "../types.dart";
import "./data.dart";

const userOptionsPrefix = "user.options";

/// Service for handling requests to the settings.
///
/// This service maintains one or more connections for the current settings
class ConfigService extends Service {
    AppConfig _cfg;
    DataService dataSvc;

    ConfigService({this.dataSvc}) : super();

    @override
    init() {
        // TODO: initialize current cfg from SQLite from SQLite (spin off an isolate?)
        // TODO: initialize all values to their defaults?
    }

    Future<AppConfig> getConfig() => Future<AppConfig>.value(_cfg);

    Future<void> setUserOption(UserOption opt, dynamic value) async {
        if (this.dataSvc == null) { throw new StateError("DataService not present"); }

        final key = "${userOptionsPrefix}.${opt.toString()}";
        await dataSvc.putKV(key, value);
        return;
    }
}
