import "dart:isolate";

import "package:event_bus/event_bus.dart";
import "package:flutter/foundation.dart";
import "package:optional/optional.dart";
import "package:http/http.dart" as http;

import "./data.dart";
import "../types.dart";
import "../errors.dart";

class ConnectionServiceEvent {}

class ConnectionAddedEvent extends ConnectionServiceEvent {
    ConnectionConfig connInfo;
    ConnectionAddedEvent(this.connInfo);
}

class ActiveConnectionChanged extends ConnectionServiceEvent {
    String value;
    ConnectionConfig conn;
    ActiveConnectionChanged(this.value, {this.conn});
}

class ConnectionServiceCommand {}

class TestConnectionCommand extends ConnectionServiceCommand {
    ConnectionConfig connInfo;

    TestConnectionCommand({this.connInfo});
}

class ConnectionServiceResponse {}

class TestConnectionResponse extends ConnectionServiceResponse {
    final bool succeeded;

    TestConnectionResponse({this.succeeded});
}

Future<TestConnectionResponse> runTestConnection(TestConnectionCommand tcc) {
    return http.get(tcc.connInfo.toUrl())
        .then((resp) => new TestConnectionResponse(succeeded: resp.statusCode == 200))
        .catchError((_) => new TestConnectionResponse(succeeded: false));
}

final kvActiveConnectionName = "ConnectionService.activeConnectionName";

// TODO: Write an isolate that sticks around and waits for messages, consider running that in one isolate
// instead of isolate-per-compute call

/// Service for handling requests to the settings.
///
/// This service maintains one or more connections for the current settings
class ConnectionService extends Service {
    Map<String, ConnectionConfig> _connections = {};
    String _activeConnectionName;
    DataService dataSvc;

    ConnectionService({this.dataSvc}) : super() {
        // TODO: setup isolates?
        // this.setupIsolates();
        this.setupEventBus();
        this.init();
    }

    ConnectionService init() {
        debugPrint("[ConnectionService] initializing...");

        // Attempt to load
        this.dataSvc
            .listEntities<ConnectionConfig>(EntityType.connection_config)
            .then((List<Entity<ConnectionConfig>> cs) {
                // Assimilate the connections
                cs.forEach((c) {
                    // Skip, if the connection is already in the local map
                    // if it is, the one in the list is probably newer
                    if (this._connections.containsKey(c.data.getName())) { return; }

                    this._connections.putIfAbsent(c.data.getName(), () => c.data);
                    debugPrint("[ConnectionService] connection [${c.data.getName()}] added from DataService");

                    this.eventBus.fire(new ConnectionAddedEvent(c.data));
                });

            })
            .then((_) => this._loadActiveConnection());

        // Attempt to load

        return this;
    }

    /// Load the active connection name
    void _loadActiveConnection() async {
        await this.dataSvc
            .getKV(kvActiveConnectionName)
            .then((maybeValue) {
                maybeValue.ifPresent((v) {
                    if (v != null) {
                        this._activeConnectionName = v;
                        debugPrint("[ConnectionService] Active connection set to: [${kvActiveConnectionName}]");

                        this.eventBus.fire(new ActiveConnectionChanged(v, conn: this._connections[v]));
                    }
                });

            });
    }

    /// Load the active connection name
    void _saveActiveConnection() async {
        await this.dataSvc
            .putKV(kvActiveConnectionName, this._activeConnectionName)
            .then((v) {
                debugPrint("[ConnectionService] Active connection saved: [${v}]");
            });
    }

    // void setupIsolates() {
    //     // Setup the general isolate
    //     ReceivePort rp = new ReceivePort();
    //     IsolateAndPort iap = new IsolateAndPort(isolate: Isolate(rp.sendPort), rp: rp);

    //     this.isolates.putIfAbsent(
    //         ServiceIsolateType.general.toString(),
    //         () => iap
    //     );
    // }

    void setupEventBus() {
        this.eventBus = new EventBus();
        // TODO: isolate stuff?
        // this.eventBus
        //     .on<TestConnectionCommand>()
        //     .listen(this.forwardToIsolate);

        // TODO: register more commands/events
    }

    // Future<IsolateAndPort> getIsolateAndPortForCommand(ConnectionServiceCommand csm) {
    //     if (csm is TestConnectionCommand && this.isolates.containsKey(ServiceIsolateType.general)) {
    //         return Future.value(this.isolates[ServiceIsolateType.general]);
    //     }

    //     return Future.error(new NoSuchIsolateError());
    // }

    // Future<ConnectionServiceResponse> forwardToIsolate(ConnectionServiceCommand cmd, {waitForResponse: false}) {
    //     return this
    //         .getIsolateAndPortForCommand(cmd)
    //         .then((iap) {
    //             // TODO: send the message to the isolate
    //             // TDOO: message it
    //             // TODO: wait for response if waitForResponse is set
    //         });
    // }

    Future<Optional<ConnectionConfig>> getConnectionByName(String name) async {
        if (!this._connections.containsKey(name)) {
            return Future.value(Optional.ofNullable(null));
        }

        return Future.value(Optional.of(this._connections[name]));
    }

    Future<List<ConnectionConfig>> getConnections() => Future.value(this._connections.values.toList());

    Future<TestConnectionResponse> testConnection(ConnectionConfig connInfo) {
        // TODO: Figure out whether event bus usage is worth (vs direct access to services)
        // this.eventBus.fire(new TestConnectionCommand(c));

        // TODO: Figure out whether manual isolate management is worth it

        // Figure out of compute needs to be used for network tasks
        // return compute(
        //     runTestConnection,
        //     new TestConnectionCommand(connInfo: c),
        // );

        return connInfo.toUrl()
            .then((url) => http.get(url))
            .then((resp) => new TestConnectionResponse(succeeded: resp.statusCode == 200))
            .catchError((err) {
                debugPrint("[ConnectionService] ERROR:\n$err");
                return new TestConnectionResponse(succeeded: false);
            });
    }

    Future<ConnectionConfig> saveConnection(ConnectionConfig connInfo) => this._addConnection(connInfo);

    Future<ConnectionConfig> _addConnection(ConnectionConfig c) async {
        this._connections[c.getName()] = c;
        debugPrint("[ConnectionService] new connection [${c.getName()}] added!");

        // Save the connection long term if a dataSvc is present is attached
        if (this.dataSvc != null) {
            await dataSvc.persistEntity(new Entity(c));
        }

        this.eventBus.fire(new ConnectionAddedEvent(c));
        return c;
    }

    Future<ConnectionConfig> setActiveConnection(ConnectionConfig c) async {
        if (!this._connections.containsKey(c.getName())) {
            await this._addConnection(c);
        }

        // Ensure that connections that aren't present
        this._activeConnectionName = c.getName();
        await _saveActiveConnection();

        return c;
    }

    Future<Optional<ConnectionConfig>> getActiveConnection() async {
        if (this._activeConnectionName == null) {
            return Future.value(Optional.ofNullable(null));
        }

        return this.getConnectionByName(this._activeConnectionName);
    }

}
