import "dart:convert";
import "dart:io";

import "package:sqflite/sqflite.dart";
import "package:path/path.dart" as p;
import "package:optional/optional.dart";
import "package:uuid/uuid.dart";

import "../types.dart";
import "../errors.dart";
import "../db/migrations.dart" as db_migrations;

const String defaultDBFileName = "rhymote.db";
const String kvTableName = "kv";
const int defaultExpectedDBVersion = 2;

/// The data service holds all long-lived entities/data for the application
///
class DataService extends Service {
    String dbFileName;
    String dbFullPath;
    num expectedDBVersion;
    Database _db;

    DataService({
        this.dbFileName = defaultDBFileName,
        this.expectedDBVersion = defaultExpectedDBVersion,
        this.dbFullPath
    }): super();

    /// Connect to the database
    Future<Database> _connectDB() async {
        // Determine the db location
        final String path = await this._getDBPath();

        this._db = await openDatabase(
            path,
            // specifying version basically doesn't do anything, except allow *onCreate*
            version: this.expectedDBVersion,
            onCreate: (Database db, _) async {
                final currentVersion = await db.getVersion();

                // @ first ever DB creation the version is expected to be 0
                // https://www.sqlite.org/pragma.html#pragma_user_version
                print("Current DB version: ${currentVersion}");
                print("Expected DB version: ${this.expectedDBVersion}");

                // migrate until version matches
                final pendingMigrations = db_migrations.migrations.where((m) => m.version > currentVersion && m.version <= this.expectedDBVersion);
                print("Found [${pendingMigrations.length}] migrations to run to reach version [${this.expectedDBVersion}]");

                // Perform all pending migrations
                for (final m in pendingMigrations) {
                    try {
                        await this._performMigration(m, db);
                    } catch (err) {
                        // TODO: do more to properly handle database exception, lots of logging?
                        print("Error while migrating DB:\n ${err.toString()}");
                        break;
                    }
                }

                // Check if we've successfully updated to the expected version
                int updatedVersion = await db.getVersion();
                if (this.expectedDBVersion != updatedVersion) {
                    print("Database was not updated to expected version [${this.expectedDBVersion}], stopped at [${updatedVersion}]");
                    // TODO: go to error page? alert?
                }

                // TODO: Print to some sort of in-device debug/event log? Better logging statement (include component at least)
                print("Database successfully migrated");
            });
    }

    /// Utility function for performing a single migration on a given database
    Future<void> _performMigration(DBMigration m, Database db) async {
        // Attempt to migrate the database and update the version inside a transaction
        await db.transaction((tx) async {
            await tx.rawQuery(m.sql);
            await tx.rawQuery("PRAGMA user_version = ${m.version};");
        });
    }

    /// Utility function for getting the current database instance
    /// This utility function is used everywhere, to ensure that the database is loaded/setup @ first use, not at service creation
    Future<Database> _getOrConnectDB() async {
        if (this._db != null) { return this._db; }

        await this._connectDB();
        return this._db;
    }

    /// Save an object
    Future<T> putKV<T>(String key, T value) async {
        // Encode the object into json
        final stringified = json.encode(value);

        // Connect to the DB and insert hte object
        final Database db = await this._getOrConnectDB();
        await db.rawInsert("INSERT OR REPLACE INTO $kvTableName (key,value) VALUES (?,?)", [key, stringified]);

        return value;
    }

    /// Retrieved a saved key-value
    Future<Optional<T>> getKV<T>(String key) async {
        final Database db = await this._getOrConnectDB();

        // Retrieve the matching rows (only expect one)
        List<Map> results = await db.rawQuery("SELECT * FROM $kvTableName WHERE key=?", [key]);
        if (results.isEmpty) { return empty; }

        // Decode map into object
        // TODO: This is dumb... shouldn't have to encode the map then decode it?
        final T decoded = json.decode(results.first["value"]);
        return Optional<T>.of(decoded);
    }

    /// Persist an entity
    Future<Entity<E>> persistEntity<E>(Entity<E> entity) async {
        final Database db = await this._getOrConnectDB();
        EntityType et = entity.getEntityType();

        if (et == null) {
            return Future.error(new UnexpectedError(cause: "EntityType could not be retrieved for a entity:\n ${entity.toString()}"));
        }

        // Get columns
        // TODO: pull this out, in a way that actually looks good. wrapping function calls in try catch
        // I would kill for haskell's monads or rust's ? operator right now.
        final columns = entity.insertColumns();
        if (columns == null) {
            return Future.error(new UnexpectedError(cause: "insert columns missing for entity [${et.toString()}]"));
        }

        // Get column values
        final values = entity.insertValues();
        if (values == null) {
            return Future.error(new UnexpectedError(cause: "insert values missing for entity [${et.toString()}]"));
        }

        // Get table name
        final tableName = getTableNameForEntityType(et);
        if (tableName == null) { return Future.error(new UnexpectedError(cause: "tableName missing for entity [${et.toString()}]")); }

        // Gather the columns to insert, preprend "uuid"
        final columnNames = columns.join(",");

        // create question marks to use in query
        final questionMarks = values.map((v) => "?").join(",");

        await db.rawInsert("INSERT INTO $tableName ($columnNames) VALUES ($questionMarks)", values);
        return entity;
    }

    /// retrieve a saved entity by Id
    Future<Optional<Entity<E>>> getEntityById<E>(String typeEnum, EntityId<E> id) async {
        final Database db = await this._getOrConnectDB();

        // Get columns
        final columns = insertColumnsForTypeEnum(typeEnum);
        if (columns == null) {
            return Future.error(new UnexpectedError(cause: "insert columns missing for type [${typeEnum.toString()}]"));
        }
        columns.insert(0, "uuid");

        // Get table name
        final tableName = getTableNameForTypeEnum(typeEnum);
        if (tableName == null) { return Future.error(new UnexpectedError(cause: "tableName missing for type [${typeEnum.toString()}]")); }

        final columnNames = columns.join(",");

        List<Map> results = await db.rawQuery("SELECT $columnNames FROM $tableName WHERE uuid=?", [id.uuid]);
        if (results.isEmpty) { return empty; }

        return Optional.of(new Entity(
            json.decode(json.encode(results.first)),  // FIX: dart:convert JSON decode/encode hack
            id: results.first["uuid"],
        ));
    }

    /// Retrieve a listing of all entities in the database
    Future<List<Entity<E>>> listEntities<E>(EntityType et, {pagination: PaginationOptions}) async {
        final Database db = await this._getOrConnectDB();

        // Create a proxy & gather table information
        final tableName = getTableNameForEntityType(et);

        // TODO: Take pagination options into account
        List<Map> results = await db.rawQuery("SELECT * FROM $tableName");

        // FIX: dart:convert JSON decode/encode hack
        return results
        // Create new entities
            .map((row) {
                // The object that comes back may have a dynamic type,
                // prefer it if present, or use the entity type that was passed in
                final typeName = row.containsKey("_type") ? row["_type"] : et.toString();

                return new Entity<E>(
                    getRowConstructorForTypeEnum(typeName)(row),
                    id: new EntityId<E>(uuid: row["uuid"])
                );
            })
            .toList();
    }

    Future<String> _getDBPath() async{
        if (this.dbFullPath != null) {
            return Future.value(this.dbFullPath);
        }

        final databasesPath = await getDatabasesPath();
        return p.join(databasesPath, this.dbFileName);
    }

    Future<void> resetDB() async {
        final path = await this._getDBPath();

        // Delete the DB file
        print("Deleting DB file @ [${path}]");
        var dbFile = new File(path);
        try {
            await dbFile.delete();
        } catch (FileSystemException) {
            // TODO: check and properly handle FileSystemException no such file
            // https://api.dartlang.org/stable/2.0.0/dart-io/FileSystemException-class.html
        }

        // Reset this._db to trigger DB creation @ next use
        this._db = null;
    }

}
